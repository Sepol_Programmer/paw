const express = require('express');
const router = express.Router();
const Verification = require("../controllers/verifyTokens.js");
const userInfoController = require("../controllers/userInfoController.js");
const adminController = require('../controllers/adminController.js');

router.post('/testsperday', (req, res, next) => { Verification(req, res, next, process.env.adminrole) }, adminController.getTestsPerDay);
router.get('/testsperperson/:userId', (req, res, next) => { Verification(req, res, next, process.env.adminrole) }, adminController.getTestsPerPerson);
router.get('/infected', (req, res, next) => { Verification(req, res, next, process.env.adminrole) }, adminController.getNumberInfected);
router.post("/requestnome", (req, res, next) => { Verification(req, res, next, process.env.adminrole) }, adminController.RequestePacienteName)
router.post("/requestdata", (req, res, next) => { Verification(req, res, next, process.env.adminrole) }, adminController.RequestePacienteData)
router.post("/pacienteenome", (req, res, next) => { Verification(req, res, next, process.env.adminrole) }, adminController.PacienteName)
router.post("/pacienteemail", (req, res, next) => { Verification(req, res, next, process.env.adminrole) }, adminController.PacienteEmail)
router.post("/testetec", (req, res, next) => { Verification(req, res, next, process.env.adminrole) }, adminController.TestesTecnico)
router.post("/testeprioridade", (req, res, next) => { Verification(req, res, next, process.env.adminrole) }, adminController.TestesPrioridade)
router.param('userId', userInfoController.getByIdUser);

module.exports = router;