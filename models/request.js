const mongoose = require('mongoose');
const Schema = mongoose.Schema;
//Schema dos pedidos
var RequestSchema = new Schema({
    name: {
        type: String,
        required: false
    },
    time: {
        type: Date,
        default: Date.now
    },
    description: {
        type: String,
        required: true
    },
    code: {
        type: String,
        required: false
    },
    userid: {
        type: String,
        required: false
    }
});

module.exports = mongoose.model('Request', RequestSchema);