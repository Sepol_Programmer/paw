const mongoose = require('mongoose');
const Schema = mongoose.Schema;


var UserSchema = new Schema({
    name: {
        type: String,
        required: true
      },
    sex: {
        type: String,
        required: true
    },
    age: {
        type: Number,
        required: true,
        min:1
    },
    contact: {
        type: Number,
        required: true,
        maxlength: 9,
        minlength: 9
    },
    address: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    diseases: {
        type: String,
        required: false
    },
    password: {
        type: String,
        required: true
    },
    role: {
        type: Number,
        default: 0
    },
    //0 não infetado, 1 supeito, 2 infetado 
    status: {
        type: Number,
        min: 0,
        max: 2,
        default: 1
    }
});

module.exports = mongoose.model('UserInfo', UserSchema);