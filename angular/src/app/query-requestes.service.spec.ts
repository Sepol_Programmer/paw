import { TestBed } from '@angular/core/testing';

import { QueryRequestesService } from './query-requestes.service';

describe('QueryRequestesService', () => {
  let service: QueryRequestesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(QueryRequestesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
