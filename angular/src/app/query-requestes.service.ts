import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class QueryRequestesService {

  constructor(private http: HttpClient) { }
  queryRequestData(date) : Observable<any>{
    return this.http.post<any>("http://localhost:3000/admin/requestdata" , {date} )
  }

  queryReqiestNome(name): Observable<any>{
    return this.http.post<any>("http://localhost:3000/admin/requestnome" , {name} )
  } 
}
