import { Component, OnInit, Input, Query } from '@angular/core';
import { Router } from '@angular/router';
import { QueryPacienteService } from '../query-paciente.service';

@Component({
  selector: 'app-query-paciente',
  templateUrl: './query-paciente.component.html',
  styleUrls: ['./query-paciente.component.css']
})
export class QueryPacienteComponent implements OnInit {
  @Input() paciente
  @Input() input
  resultado: any = []
  visivelNome = false;
  visivelEmail = false;

  displayedColumns: string[] = ["paciente_coluna", "sex_coluna", "age_coluna","address_coluna","estado_coluna","email_coluna"];
  displayedColumns2: string[] = ["email_coluna","address_coluna","paciente_coluna", "sex_coluna", "age_coluna","estado_coluna"];
  constructor(private router: Router, private PacienteService: QueryPacienteService) { }

  ngOnInit(): void {
  }
Paciente(): void {
    if (this.paciente == "Nome") {
      this.visivelEmail = false;
      this.visivelNome = true;
      this.PacienteService.queryPacienteNome(this.input).subscribe((tec: any) => {
        this.resultado = tec
      })

     
    } else if (this.paciente == "Email") {
      this.visivelNome = false;
      this.visivelEmail = true;
      this.PacienteService.queryPacienteEmail(this.input).subscribe((tec: any) => {
        this.resultado = tec
      })
     

      
    }
  }
}
