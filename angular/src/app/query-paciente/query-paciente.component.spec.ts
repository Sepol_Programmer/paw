import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QueryPacienteComponent } from './query-paciente.component';

describe('QueryPacienteComponent', () => {
  let component: QueryPacienteComponent;
  let fixture: ComponentFixture<QueryPacienteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QueryPacienteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QueryPacienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
