import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private http: HttpClient) { }

  login(email: string, password: string): Observable<any> {
    return this.http.post<any>('http://localhost:3000/usersinfo/login', { email, password })
  }

  logout() {
    // removee user from local storage to log user out
    localStorage.removeItem('currentUser');
  }

  register(name: string, sex: string, age: number, contact: number, address: string, email: string, password: string, diseases: string): Observable<any> {
    return this.http.post<any>('http://localhost:3000/usersinfo/create', { name, sex, age, contact, address, email, password, diseases });
  }
}
