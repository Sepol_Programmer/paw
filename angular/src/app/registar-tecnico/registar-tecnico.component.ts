import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import {RegistartecnicoService}  from  '../services/registartecnico.service';
import { FormControl, Validators } from '@angular/forms';
@Component({
  selector: 'app-registar-tecnico',
  templateUrl: './registar-tecnico.component.html',
  styleUrls: ['./registar-tecnico.component.css']
})
export class RegistarTecnicoComponent implements OnInit {
  hide = true;

  /* Contacto nullo, password merda, sexo também */
  sex;

  @Input() password;

  /* Nome */
  name = new FormControl('', [Validators.required]);

  /* Email */
  email = new FormControl('', [Validators.required, Validators.email]);

  /* Morada */
  address = new FormControl('', [Validators.required]);

  /* Idade */
  age = new FormControl('', [Validators.required]);

  /* Contacto */
  contact = new FormControl('', [Validators.required]);

/* */
role = new FormControl('', [Validators.required]);

  /* Erro */
  getErrorMessage() {
    if (this.email.hasError('required') || this.address.hasError('required')) {
      return 'Preenchimento Obrigatório';
    }
    return this.email.hasError('email') ? 'O email dado não é válido' : '';
  }
  constructor( private router: Router, private authService : RegistartecnicoService) { }

  ngOnInit(): void {
 
  }
  registerTec(): void {
    this.authService.registerTec(this.name.value, this.sex, this.age.value, this.contact.value, this.address.value, this.email.value, this.password,this.role.value).subscribe((authData: any) => {
           
      }
    )
  }

  updateAdmin():void{
    this.authService.updateAdmin(this.password).subscribe((data: any) => {
      console.log(this.password);
    }

    )}

}
