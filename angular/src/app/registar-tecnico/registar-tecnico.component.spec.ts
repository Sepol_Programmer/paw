import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistarTecnicoComponent } from './registar-tecnico.component';

describe('RegistarTecnicoComponent', () => {
  let component: RegistarTecnicoComponent;
  let fixture: ComponentFixture<RegistarTecnicoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistarTecnicoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistarTecnicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
