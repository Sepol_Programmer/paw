import { Component, OnInit } from '@angular/core';
import { UserDataService } from '../services/user-data.service';

@Component({
  selector: 'app-mytectests',
  templateUrl: './mytectests.component.html',
  styleUrls: ['./mytectests.component.css']
})
export class MytectestsComponent implements OnInit {

  tests: any = [];
  displayedColumns: string[] = ['code', 'technician', 'priority', 'date', 'teststatus', 'result', 'update'];

  constructor(private userdata: UserDataService) { }

  ngOnInit(): void {
    this.getTecTests();
  }

  getTecTests(): void {
    this.userdata.getTecTests().subscribe((data: any) => {
      this.tests = data;
    });
  }
}
