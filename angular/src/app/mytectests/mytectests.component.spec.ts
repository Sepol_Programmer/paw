import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MytectestsComponent } from './mytectests.component';

describe('MytectestsComponent', () => {
  let component: MytectestsComponent;
  let fixture: ComponentFixture<MytectestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MytectestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MytectestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
