import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class QueryService {

  constructor(private http : HttpClient) { }

  queryTest(tecnico){
    return this.http.post<any>("http://localhost:3000/admin//testetec" , {tecnico})
  }

  queryPrio(priority){
    return this.http.post<any>("http://localhost:3000/admin/testeprioridade" , {priority} )
  }


}
