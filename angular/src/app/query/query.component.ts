import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { QueryService } from "../query.service"

@Component({
  selector: 'app-query',
  templateUrl: './query.component.html',
  styleUrls: ['./query.component.css']
})

export class QueryComponent implements OnInit {
  @Input() tecnico
  @Input() input
  resultado: any = []
  visivelTec = false;
  visivelPrio = false;

  displayedColumns: string[] =  ['tecnico_coluna', "paciente_coluna","prioridade_coluna","data_coluna","codigo.coluna"];
  displayedColumns2: string[] = ["prioridade_coluna",'tecnico_coluna', "paciente_coluna","data_coluna","codigo.coluna"];

  constructor(private router: Router, private queryService: QueryService) {
  }

  ngOnInit(): void {

  }


  testeTecn(): void {
    if (this.tecnico == "Nome") {
      this.visivelPrio = false;
      this.visivelTec = true;
      this.queryService.queryTest(this.input).subscribe((tec: any) => {
        this.resultado = tec
      })
    } else if (this.tecnico == "Prioridade") {
      this.visivelTec = false;
      this.visivelPrio = true;
      this.queryService.queryPrio(this.input).subscribe((tec: any) => {
        this.resultado = tec
      }

      )
    }
  }
}
