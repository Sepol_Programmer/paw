import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserDataService } from '../services/user-data.service';
import { ThrowStmt } from '@angular/compiler';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog';
import { CreaterequestComponent } from '../createrequest/createrequest.component';
import { AuthenticationService } from '../helper/authentication.service';
import { RegistarTecnicoComponent } from '../registar-tecnico/registar-tecnico.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  currentUser: any;
  role0: boolean;
  role1: boolean;
  role2: boolean;

  constructor(private router: Router, private auth: AuthenticationService) {}

  ngOnInit(): void {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if(this.currentUser.user.role == 0){
      this.role0 = true;
      this.role1= false;
      this.role2= false;
    }else if(this.currentUser.user.role == 1){
      this.role1 = true;
      this.role0= false;
      this.role2= false;
    }else if(this.currentUser.user.role == 2){
      this.role2 = true;
      this.role0= false;
      this.role1= false;
    }
  }

  logout(): void{
    this.auth.logout();
    this.router.navigate(['/login']);
  }
}
