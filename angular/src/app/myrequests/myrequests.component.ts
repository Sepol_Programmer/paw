import { Component, OnInit } from '@angular/core';
import { UserDataService } from '../services/user-data.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { CreaterequestComponent } from '../createrequest/createrequest.component';

@Component({
  selector: 'app-myrequests',
  templateUrl: './myrequests.component.html',
  styleUrls: ['./myrequests.component.css']
})
export class MyrequestsComponent implements OnInit {

  requests: any = [];
  displayedColumns2: string[] = ['code', 'time', 'description', 'remove'];

  constructor(private userdata: UserDataService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.getuserRequests();
  }

  getuserRequests(): void {
    this.userdata.getuserRequests().subscribe((data: {}) => {
      this.requests = data;
      console.log(this.requests);
    })
  }

  createRequest(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    const dialogRef = this.dialog.open(CreaterequestComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(description => {
      if (description != undefined) {
        this.userdata.postuserRequest(description).subscribe(result => {
          console.log(result);
          //refresh
          window.location.reload();
        })
      }
    })
  }

  removeRequest(code: string): void {
    this.userdata.removeRequest(code).subscribe((data: any) => {
      window.location.reload();
    });
  }
}
