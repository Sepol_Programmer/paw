import { Component, OnInit, Input } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserDataService } from '../services/user-data.service';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-userdataupdate',
  templateUrl: './userdataupdate.component.html',
  styleUrls: ['./userdataupdate.component.css']
})
export class UserdataupdateComponent implements OnInit {
  hide = true;

  @Input() sex;
  @Input() diseases;
  @Input() password;
  @Input() name;
  @Input() address;
  @Input() age;
  @Input() contact;
  currentUser: any;

  /* Email */
  email = new FormControl('', [Validators.email]);

  /* Erro */
  getErrorMessage() {
    return this.email.hasError('email') ? 'O email dado não é válido' : '';
  }
  constructor(private router: Router, private userdata: UserDataService) { }

  ngOnInit(): void {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

  }

  update(): void {
    const id = this.currentUser.user._id;
    const token = this.currentUser.token;
    var newemail = this.email.value;

    if (this.password == "") {
      this.password = undefined;
    }
    if (this.email.value == "") {
      newemail = undefined;
    }
    if (this.address == "") {
      this.address = undefined;
    }
    if (this.age == null) {
      this.age = undefined;
    }
    if (this.diseases == "") {
      this.diseases = undefined;
    }
    if (this.name == "") {
      this.name = undefined;
    }
    
    this.userdata.updateuserData(id, this.name, this.sex, this.age, this.contact, this.address, newemail, this.password, this.diseases).subscribe((modifiedData: any) => {

      var newCurrentUser = {
        user: modifiedData,
        token: token
      }

      localStorage.setItem('currentUser', JSON.stringify(newCurrentUser));
      console.log(newCurrentUser);
      this.router.navigate(['']);
    });
  }

  cancel(): void {
    this.router.navigate(['']);
  }
}
