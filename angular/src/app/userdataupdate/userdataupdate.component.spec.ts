import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserdataupdateComponent } from './userdataupdate.component';

describe('UserdataupdateComponent', () => {
  let component: UserdataupdateComponent;
  let fixture: ComponentFixture<UserdataupdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserdataupdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserdataupdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
