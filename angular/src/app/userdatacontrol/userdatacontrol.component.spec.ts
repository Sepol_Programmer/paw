import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserdatacontrolComponent } from './userdatacontrol.component';

describe('UserdatacontrolComponent', () => {
  let component: UserdatacontrolComponent;
  let fixture: ComponentFixture<UserdatacontrolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserdatacontrolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserdatacontrolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
