import { Component, OnInit } from '@angular/core';
import { UserDataService } from '../services/user-data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-userdatacontrol',
  templateUrl: './userdatacontrol.component.html',
  styleUrls: ['./userdatacontrol.component.css']
})
export class UserdatacontrolComponent implements OnInit {

  info: any = [];

  constructor(private router: Router, private userdata: UserDataService) { }

  ngOnInit(): void {
    this.getuserData();
  }

  getuserData(): void {
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    const id = currentUser.user._id;
    this.userdata.getuserData(id).subscribe((data: {}) => {
      this.info = data;
      console.log(this.info)
    });
  }

  edit(): void {
    this.router.navigate(['/myprofile']);
  }
}
