import { Component, OnInit } from '@angular/core';
import { UserDataService } from '../services/user-data.service';

@Component({
  selector: 'app-alltests',
  templateUrl: './alltests.component.html',
  styleUrls: ['./alltests.component.css']
})
export class AlltestsComponent implements OnInit {

  tests: any = [];
  displayedColumns: string[] = ['code', 'technician', 'priority', 'date', 'teststatus', 'result', 'update'];

  constructor(private userdata: UserDataService) { }

  ngOnInit(): void {
    this.getAllTests();
  }

  getAllTests():void{
   this.userdata.getAllTests().subscribe((data: any) => {
    this.tests = data;
   });
  }
}
