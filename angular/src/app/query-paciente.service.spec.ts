import { TestBed } from '@angular/core/testing';

import { QueryPacienteService } from './query-paciente.service';

describe('QueryPacienteService', () => {
  let service: QueryPacienteService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(QueryPacienteService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
