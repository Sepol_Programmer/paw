import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../helper/authentication.service';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-regist',
  templateUrl: './regist.component.html',
  styleUrls: ['./regist.component.css']
})
export class RegistComponent implements OnInit {
  hide = true;
  
  @Input() sex;
  @Input() diseases;
  @Input() password;

  /* Nome */
  name = new FormControl('', [Validators.required]);

  /* Email */
  email = new FormControl('', [Validators.required, Validators.email]);

  /* Morada */
  address = new FormControl('', [Validators.required]);

  /* Idade */
  age = new FormControl('', [Validators.required]);

  /* Contacto */
  contact = new FormControl('', [Validators.required]);

  /* Erro */
  getErrorMessage() {
    if (this.email.hasError('required') || this.address.hasError('required')) {
      return 'Preenchimento Obrigatório';
    }
    return this.email.hasError('email') ? 'O email dado não é válido' : '';
  }

  constructor(private router: Router, private authService: AuthenticationService) { }

  ngOnInit(): void {
  }

  register(): void {
    this.authService.register(this.name.value, this.sex, this.age.value, this.contact.value, this.address.value, this.email.value, this.password, this.diseases).subscribe((authData: any) => {
      if (authData && authData.token) {
        //store user details and jwt token in local storage to keep user logged in between page refreshes
        localStorage.setItem('currentUser', JSON.stringify(authData));
        this.router.navigate(['']);
      }
    })
  }
}
