import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from './services/auth-guard.service';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { RegistComponent } from './regist/regist.component';
import { UserdatacontrolComponent } from './userdatacontrol/userdatacontrol.component';
import { UserdataupdateComponent } from './userdataupdate/userdataupdate.component';
import { UsersComponent } from './users/users.component';
import {QueryComponent } from './query/query.component';
import { QueryPacienteComponent } from './query-paciente/query-paciente.component';
import { QueryRequestesComponent } from './query-requestes/query-requestes.component';

const routes: Routes = [

  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: '',
    component: HomeComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'register',
    component: RegistComponent
  },
  {
    path: 'myprofile',
    component: UserdataupdateComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'tecteste',
    component: QueryComponent,
      },

      {
        path: 'paciente',
        component: QueryPacienteComponent,
          },
          {
            path: 'requestes',
            component:QueryRequestesComponent,
              },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
