import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class QueryPacienteService {

  constructor(private http: HttpClient) { }
  queryPacienteEmail(email): Observable<any>{
    return this.http.post<any>("http://localhost:3000/admin/pacienteemail" , {email} )
  }

  queryPacienteNome(name): Observable<any>{
    return this.http.post<any>("http://localhost:3000/admin/pacienteenome" , {name} )
  }
}
