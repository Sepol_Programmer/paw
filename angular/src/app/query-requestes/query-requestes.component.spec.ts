import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QueryRequestesComponent } from './query-requestes.component';

describe('QueryRequestesComponent', () => {
  let component: QueryRequestesComponent;
  let fixture: ComponentFixture<QueryRequestesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QueryRequestesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QueryRequestesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
