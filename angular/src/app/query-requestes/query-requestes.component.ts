import { Component, OnInit, Input, Query } from '@angular/core';
import { Router } from '@angular/router';
import { QueryRequestesService } from '../query-requestes.service';
import { analyzeAndValidateNgModules } from '@angular/compiler';


@Component({
  selector: 'app-query-requestes',
  templateUrl: './query-requestes.component.html',
  styleUrls: ['./query-requestes.component.css']
})
export class QueryRequestesComponent implements OnInit {

  @Input() paciente
  @Input() input
  @Input() inputData
  resultado: any = []
  visivelNome = false;
  visivelData = false;
  visivelInputNome = false;
  visivelInputData = false;
  inputdatedia:any
  inputdatemes:any
  inputdatano:any


  displayedColumns: string[] = ["request_coluna", "description_coluna", "time_coluna", "code_coluna"];
  displayedColumns2: string[] = ["time_coluna","request_coluna", "description_coluna",  "code_coluna"];
  constructor(private router: Router, private RequestService: QueryRequestesService) { }

  ngOnInit(): void {
  }


  Escolha() {
    if (this.paciente == "Nome") {
      this.visivelInputData = false;
      this.visivelInputNome = true;
    } else if (this.paciente == "Data") {
      this.visivelInputData = true;
      this.visivelInputNome = false;
    }
  }


  Testar() {
    console.log("data", this.inputData.getDay())
    console.log(this.resultado)
  }

  Requests(): void {
    if (this.paciente == "Nome") {
      this.visivelData = false;
      this.visivelNome = true;
      this.RequestService.queryReqiestNome(this.input).subscribe((tec: any) => {
        this.resultado = tec
      })


    } else if (this.paciente == "Data") {
      this.visivelNome = false;
      this.visivelData = true;
    
      
      console.log("//////////////////////////////////////data", this.inputData)
      this.RequestService.queryRequestData(new Date(this.inputData)).subscribe((tec: any) => {
        console.log (tec)
        this.resultado = tec
      })



    }
  }

}
