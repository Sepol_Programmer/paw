import { Component, OnInit } from '@angular/core';
import { UserDataService } from '../services/user-data.service';

@Component({
  selector: 'app-mytests',
  templateUrl: './mytests.component.html',
  styleUrls: ['./mytests.component.css']
})
export class MytestsComponent implements OnInit {

  tests: any = [];
  displayedColumns: string[] = ['code', 'technician', 'priority', 'date', 'teststatus', 'result'];

  constructor(private userdata: UserDataService) { }

  ngOnInit(): void {
    this.getuserTests();
  }
  getuserTests(): void {
    this.userdata.getuserTests().subscribe((data: {}) => {
      this.tests = data;
      console.log(this.tests);
    });
  }
}
