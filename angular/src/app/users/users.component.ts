import { Component, OnInit } from '@angular/core';
import { UsersService } from "../services/users.service"
import { Router } from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  userslist: any = [];
  displayedColumns: string[] = ['name', 'sex', 'age', 'contact', 'address', 'email', 'diseases', 'password', 'role', 'status', 'delete'];
  constructor(private router: Router, private users: UsersService) { }

  ngOnInit(): void {
    this.getUsersList()
  }


  getUsersList(): void {
    console.log("aqui")
    this.users.getUsers().subscribe((data: {}) => {
      console.log("aqui2")
      this.userslist = data;
      console.log(this.userslist);
    }
    )
  }

  deleteUser(id: string): void {
    console.log("aqui1",id)
    this.users.deleteUser(id).subscribe((data: any) => {
      console.log("aqui3",id)
      window.location.reload();
    });
  }
}
