import { Component, OnInit } from '@angular/core';
import { UserDataService } from '../services/user-data.service';
import { MatDialogConfig, MatDialog } from '@angular/material/dialog';
import { ReqtotestComponent } from '../reqtotest/reqtotest.component';

@Component({
  selector: 'app-allrequests',
  templateUrl: './allrequests.component.html',
  styleUrls: ['./allrequests.component.css']
})
export class AllrequestsComponent implements OnInit {

  requests: any = [];
  displayedColumns: string[] = ['code', 'name', 'time', 'description', 'accept'];

  constructor(private userdata: UserDataService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.getAllRequests();
  }

  getAllRequests(): void {
    this.userdata.getAllRequests().subscribe((data: any) => {
      this.requests = data;
    });
  }


  acceptRequest(code: string): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    const dialogRef = this.dialog.open(ReqtotestComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(data => {
      console.log(data.date);
      console.log(data.priority);
      if (data.date != undefined || data.priority != undefined) {
        this.userdata.acceptRequest(code, data.date, data.priority).subscribe(result => {
          console.log(result);
          //refresh
          this.getAllRequests();
        })
      }
    })
  }
}
