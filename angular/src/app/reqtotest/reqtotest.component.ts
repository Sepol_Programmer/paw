import { Component, OnInit, Input } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-reqtotest',
  templateUrl: './reqtotest.component.html',
  styleUrls: ['./reqtotest.component.css']
})
export class ReqtotestComponent {

  date = new FormControl(new Date().toISOString());
  @Input() priority;

  constructor(private dialogRef: MatDialogRef<ReqtotestComponent>) { }

  save(): void {
    const data = {
      date: this.date.value,
      priority: this.priority
    }
    this.dialogRef.close(data);
  }

  close(): void {
    this.dialogRef.close();
  }

}
