import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReqtotestComponent } from './reqtotest.component';

describe('ReqtotestComponent', () => {
  let component: ReqtotestComponent;
  let fixture: ComponentFixture<ReqtotestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReqtotestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReqtotestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
