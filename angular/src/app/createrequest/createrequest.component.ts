import { Component, OnInit, Input, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-createrequest',
  templateUrl: './createrequest.component.html',
  styleUrls: ['./createrequest.component.css']
})
export class CreaterequestComponent {

  @Input() description;

  constructor(private dialogRef: MatDialogRef<CreaterequestComponent>) { }

  save(): void {
    this.dialogRef.close(this.description);
  }

  close(): void {
    this.dialogRef.close();
  }
}
