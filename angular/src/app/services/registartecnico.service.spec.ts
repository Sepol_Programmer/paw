import { TestBed } from '@angular/core/testing';

import { RegistartecnicoService } from './registartecnico.service';

describe('RegistartecnicoService', () => {
  let service: RegistartecnicoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RegistartecnicoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
