import { Injectable } from '@angular/core';
import { Note } from '../models/note';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NotesService {
  private _notes: Note[];
  notes: BehaviorSubject<Note[]>;

  constructor() {
    this._notes = JSON.parse(localStorage.getItem('notes'));
    this._notes = this._notes == null ? [] : this._notes;
    this.notes = new BehaviorSubject<Note[]>(this._notes);
  }

  addNote(a: Note) {
    this._notes.push(a);
    //this.http.post... -> enviar ingormação para uma API
    localStorage.setItem('notes',JSON.stringify(this._notes));
    this.notes.next(this._notes);
  }

  deleteNote(index: number) {
    this._notes.splice(index, 1);
    localStorage.setItem('notes',JSON.stringify(this._notes));
    this.notes.next(this._notes);
  }

  updateNote(index: number, a: Note) {
    this._notes[index] = a;
    this.notes.next(this._notes);
  }

  getNote(index: number): Note {
    return this._notes[index];
  }
}
