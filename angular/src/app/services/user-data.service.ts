import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  })
}

@Injectable({
  providedIn: 'root'
})
export class UserDataService {

  constructor(private http: HttpClient) { }

  getuserData(id: String): Observable<any> {
    return this.http.get<any>('http://localhost:3000/usersinfo/user/' + id);
  }

  getuserTests(): Observable<any> {
    return this.http.get<any>('http://localhost:3000/tests/mytests');
  }

  getuserRequests(): Observable<any> {
    return this.http.get<any>('http://localhost:3000/requests/myrequests');
  }

  postuserRequest(description: string): Observable<any> {
    return this.http.post<any>('http://localhost:3000/requests/createRequest/', { description });
  }

  updateuserData(id: string, name: string, sex: string, age: number, contact: number, address: string, email: string, password: string, diseases: string): Observable<any> {
    return this.http.put<any>('http://localhost:3000/usersinfo/user/' + id, { name, sex, age, contact, address, email, password, diseases });
  }

  removeRequest(code: string): Observable<any> {
    return this.http.delete<any>('http://localhost:3000/requests/deleteRequest/' + code);
  }

  getAllRequests(): Observable<any> {
    return this.http.get<any>('http://localhost:3000/requests/allrequests');
  }

  getAllTests(): Observable<any> {
    return this.http.get<any>('http://localhost:3000/tests/alltests');
  }

  acceptRequest(code: string, date: Date, priority: number): Observable<any> {
    return this.http.post<any>('http://localhost:3000/requests/acceptrequest/' + code, { date, priority });
  }

  getTecTests(): Observable<any> {
    return this.http.get<any>('http://localhost:3000/tests/tectests');
  }
}
