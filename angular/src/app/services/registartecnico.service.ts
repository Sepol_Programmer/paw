import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class RegistartecnicoService {

  constructor(private http:HttpClient) {
    
   }

   registerTec(name: string, sex: string, age: number, contact: number, address: string, email: string, password: string,role : number): Observable<any> {
    return this.http.post<any>('http://localhost:3000/usersinfo/createTecnico', { name, sex, age, contact, address, email, password ,role});
  }

  updateAdmin(password:string){
    return this.http.post<any>('http://localhost:3000/usersinfo/updateAdmin',{password})
  }
}
