import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatGridListModule } from '@angular/material/grid-list';
import { FormsModule } from '@angular/forms';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { JwtInterceptorService } from './helper/jwt-interceptor.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule, MatFormField } from '@angular/material/form-field';
import { MatSliderModule } from '@angular/material/slider';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatButtonToggleModule, MatButtonToggle } from '@angular/material/button-toggle';
import { MatSelectModule } from '@angular/material/select';
import { RegistComponent } from './regist/regist.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatRadioModule } from '@angular/material/radio';
import { MatTabsModule } from '@angular/material/tabs';
import { CreaterequestComponent } from './createrequest/createrequest.component';
import { MatDialogModule } from '@angular/material/dialog';
import { UserdatacontrolComponent } from './userdatacontrol/userdatacontrol.component';
import { UserdataupdateComponent } from './userdataupdate/userdataupdate.component';
import { UsersComponent } from './users/users.component';
import { MyrequestsComponent } from './myrequests/myrequests.component';
import { AllrequestsComponent } from './allrequests/allrequests.component';
import { MytestsComponent } from './mytests/mytests.component';
import { AlltestsComponent } from './alltests/alltests.component';
import { MytectestsComponent } from './mytectests/mytectests.component';
import { RegistarTecnicoComponent } from './registar-tecnico/registar-tecnico.component';
import { ReqtotestComponent } from './reqtotest/reqtotest.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { QueryComponent } from './query/query.component';
import { QueryPacienteComponent } from './query-paciente/query-paciente.component';
import { QueryRequestesComponent } from './query-requestes/query-requestes.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    RegistComponent,
    CreaterequestComponent,
    UserdatacontrolComponent,
    UserdataupdateComponent,
    UsersComponent,
    MyrequestsComponent,
    AllrequestsComponent,
    MytestsComponent,
    AlltestsComponent,
    MytectestsComponent,
    RegistarTecnicoComponent,
    ReqtotestComponent,
    QueryComponent,
    QueryPacienteComponent,
    QueryRequestesComponent
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatGridListModule,
    FormsModule,
    MatMenuModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSortModule,
    MatTableModule,
    MatIconModule,
    MatFormFieldModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatButtonToggleModule,
    MatSelectModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatRadioModule,
    MatTabsModule,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule
  ],
  entryComponents: [
    CreaterequestComponent
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptorService,
      multi: true
    },
    MatDatepickerModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
