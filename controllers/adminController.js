const mongoose = require('mongoose');
const UserInfo = require('../models/userInfo');
const Test = require('../models/test');
const Requests = require("../models/request")

adminController = {};

adminController.getTestsPerDay = function (req, res, next) {
    var newdate1 = new Date(req.body.date);

    Test.find({ date: { "$gte": new Date(newdate1.setHours(01, 00, 00)), "$lt": new Date(newdate1.setHours(24, 59, 59)) } }, function (err, result) {
        if (err) {
            res.status(400).json(err);
        } else {
            res.status(200).json("Número de testes em " + req.body.date + " : " + result.length);
        }
    });
};

adminController.getTestsPerPerson = function (req, res, next) {
    Test.find({ userid: req.user._id }, function (err, result) {
        if (err) {
            res.status(400).json(err);
        } else {
            if (result.length == 0) {
                res.status(200).json("O user dado não fez testes.");
            } else {
                if (result.length == 1) {
                    res.status(200).json(result[0].client + " realizou " + result.length + " teste.");
                } else {
                    res.status(200).json(result[0].client + " realizou " + result.length + " testes.");
                }
            }
        }
    });
};

adminController.getNumberInfected = function (req, res, next) {
    UserInfo.find({ status: 2 }, function (err, result) {
        if (err) {
            res.status(400).json(err);
        } else {
            res.status(200).json("Número total de infetados: " + result.length);
        }
    });
};


adminController.RequestePacienteName = function (req, res, next) {
    var query = { name: req.body.name };
    Requests.find(query, function (err, result) {
        if (err) {
            res.status(400).json(err);
        } else if (result.length == 0) {
            res.status(400).json("Não existe nenhum pedido de paciente com esse nome");
        } else {
            res.status(200).json(result);

        }


    })
}


adminController.RequestePacienteData = function (req, res, next) {
    var newdate1 =new Date (req.body.date);

    Requests.find({ time: { "$gte": new Date(newdate1.setHours(01, 00, 00)), "$lt": new Date(newdate1.setHours(24, 59, 59)) } }, function (err, result) {
        if (err) {
            res.status(400).json(err);
        } else {
            res.status(200).json(result);
        }
    });
};

adminController.PacienteName = function (req, res, next) {
    var query = { name: req.body.name };
    UserInfo.find(query, function (err, result) {
        if (err) {
            res.status(400).json(err);
        } else if (result.length == 0) {
            res.status(400).json("Não existe nenhum paciente com esse nome");
        } else {
            res.status(200).json(result);

        }


    })
}
adminController.PacienteEmail = function (req, res, next) {
    var query = { email: req.body.email };
    UserInfo.find(query, function (err, result) {
        if (err) {
            res.status(400).json(err);
        } else if (result.length == 0) {
            res.status(400).json("Não existe nenhum paciente com esse email");
        } else {
            res.status(200).json(result);

        }


    })
}

adminController.TestesPrioridade = function (req, res, next) {
    var query = {priority : req.body.priority };
    Test.find(query, function (err, result) {
        if (err) {
            res.status(400).json(err);
        } else if (result.length == 0) {
            res.status(400).json("Não existe nenhum teste com essa prioridade");
        } else {
            res.status(200).json(result);

        }


    })
}


adminController.TestesTecnico = function (req, res, next) {
    var query = {technician: req.body.tecnico };
    Test.find(query, function (err, result) {
        if (err) {
            res.status(400).json(err);
        } else if (result.length == 0) {
            res.status(400).json("Não existe nenhum tecnico com esse nome");
        } else {
            res.status(200).json(result);

        }


    })
}



module.exports = adminController;
