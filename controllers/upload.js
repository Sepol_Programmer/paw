
const util = require("util");
const multer = require("multer");
const GridFsStorage = require("multer-gridfs-storage");

var storage = new GridFsStorage({
  url: " mongodb+srv://admin:admin@database-hkipt.mongodb.net/test?retryWrites=true&w=majority",
  options: { useNewUrlParser: true, useUnifiedTopology: true },
});

var uploadFile = multer({ storage: storage }).single("file");
//util.promisify = async mas melhor
var uploadFilesMiddleware = util.promisify(uploadFile);
module.exports = uploadFilesMiddleware;