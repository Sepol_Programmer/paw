const jwt = require("jsonwebtoken");

//middleware de verificação di login
function verification(req, res, next, role) {
    const token = req.header('Authorization');
    if (!token) {
        return res.status(401).json("Acesso Negado introduza um token");
    } else {
        jwt.verify(token, process.env.TOKEN_SECRET, function (err, decoded) {
            if (err) {
                return res.status(401).json("Token incorreto");
            } else {
                if (role > decoded.role) {
                    return res.status(401).json("Autenticação falhada");
                }
                req.username = decoded.name;
                req.userid = decoded._id;
                req.role = decoded.role;
                next();
            }
        })

    }

}

module.exports = verification;


